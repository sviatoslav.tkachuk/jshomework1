let name = prompt("What is your name?");

while (name == "") {
    name = prompt("What is your name?", "John Smith");
}

let age = prompt("How old are you?");

while (isNaN(age) || age == "") {
    age = prompt("How old are you?", "100");
}

if (age < 18) {
    alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
    let answer = confirm("Are you sure you want to continue?");
    if (answer) {
        alert("Welcome, " + name);
    } else {
        alert("You are not allowed to visit this website.");
    }
} else if (age > 22) {
    alert("Welcome, " + name);
}
